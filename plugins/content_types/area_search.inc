<?php
/*
NOT COMPLETE
*/
$plugin = array(
  'title' => t('Area search add / edit'),
  'description' => t('Area search form.'),
  'single' => TRUE,
/*
  'required context' => new ctools_context_required(t('User'), 'user'),
*/
  'category' => t('Sc2'),
  'icon' => 'icon_user.png',
  'render callback' => 'geofield_simplesearch_area_search_content_type_render',
  'admin title' => 'geofield_simplesearch_area_search_content_type_admin_title',
);


/**
* Output function for the 'author pane' content type.
*/
// The function name is MODULE_NAME_CT_NAME_content_type_render
function geofield_simplesearch_area_search_content_type_render($subtype, $conf, $panel_args, $context) {
	$block = NULL;
	return $block;
}


function geofield_simplesearch_area_search_content_type_admin_title($subtype, $conf, $context) {
	return t('Geofield simple search: area form');
}

/*
configuration
*/

/**
* Returns an edit form for the custom type.
*/
// The function name is MODULE_NAME_CT_NAME_content_type_edit_form

function geofield_simplesearch_area_search_content_type_edit_form(&$form, &$form_state) {
	// The current configuration
	$conf = $form_state['conf'];
	// This and the next one are normal FAPI form making.
	$form['panel_path'] = array(
	'#type' => 'textfield',
	'#title' => t('Panel path'),
	'#size' => 50,
	'#default_value' => $conf['panel_path'],
	'#prefix' => '
	',
	'#suffix' => '

	',
	);
}


function geofield_simplesearch_area_search_content_type_edit_form_submit(&$form, &$form_state) {
	// For each part of the form defined in the 'defaults' array set when you
	// defined the content type, copy the value from the form into the array
	// of items to be saved. We don't ever want to use
	// $form_state['conf'] = $form_state['values'] because values contains
	// buttons, form id and other items we don't want stored. CTools will handle
	// the actual form submission.
	foreach (array_keys($form_state['plugin']['defaults']) as $key) {
		$form_state['conf'][$key] = $form_state['values'][$key];
	}
}

