<?php


/*
 * Implementation of hook_views_handlers().
*/
 function geofield_simplesearch_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'geofield_simplesearch') .'/views',
    ),
    'handlers' => array(
      'geofield_simplesearch_handler_argument_distance_bouds' => array(
        'parent' => 'views_handler_argument',
      ),
    ),
  );
}


function geofield_simplesearch_views_data_alter(&$data) {
//	$data = array();
  foreach (field_info_fields() as $field) {
	if($field['type'] != 'geofield'){
		continue;
	}
	
	$fieldname = 'field_data_'.$field['field_name'];
	$data[$fieldname][$field['field_name']]['group'] = t('geofield simplesearch');
	$data[$fieldname][$field['field_name']]['argument'] = array(
		'handler' => 'geofield_precision_handler_argument_distance_bouds',
	);
	$data[$fieldname][$field['field_name']]['table']['join']['type'] = 'RIGHT';
  }
	return $data;
}
