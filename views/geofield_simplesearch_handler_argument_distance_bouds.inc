<?php

/**
 * Simple filter to handle greater than/less than filters
 *
 * @ingroup views_argument_handlers
 */
class geofield_precision_handler_argument_distance_bouds extends views_handler_argument {
  function title() {
	$tmp = check_plain($this->argument);
	$tmp = str_replace('_', ' ',$tmp);
	$tmp = str_replace('--', ', ',$tmp);
	$square = _geofield_simplesearch_geocoding( $tmp );
    return isset($square['name']) ? $square['name'] : $tmp;
  }
  function get_title() {
    return $this->title();
  }
  
  function query($group_by = FALSE) {
	$distance = 30;//$options[0]['search_distance'];
    $this->ensure_my_table();
/*
	$distance_meters = $distance / 1.609;
*/
	$distance_meters = $distance;
/*
	$latcol = $this->real_field.'_lat';
	$loncol = $this->real_field.'_lon';
*/
	$latcol = $this->real_field.'_lat';
	$loncol = $this->real_field.'_lon';
	$my_query = urldecode($this->argument);
	$my_query = check_plain($my_query);
/*
UNDO CLEAN URL
*/
	$my_query = str_replace('_',' ',$my_query);
	$my_query = str_replace('--',', ',$my_query);
	$square = NULL;
	if( strlen( $my_query ) > 1 ) {
		$square = _geofield_simplesearch_geocoding( $my_query );		
	}
	if( $square != NULL ){
		$this->query->add_where_expression(0, '((ACOS(SIN(' . floatval($square['lat']) . ' * PI() / 180) * SIN(' . $latcol . ' * PI() / 180) + COS(' . floatval($square['lat']) . ' * PI() / 180) * COS(' . $latcol . ' * PI() / 180) * COS((' . floatval($square['lon']) . ' - ' . $loncol . ') * PI() / 180)) * 180 / PI()) * 60 * 1.1515) < '. $distance_meters );
	}
  }
}
